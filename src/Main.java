import java.net.MalformedURLException;
import java.net.URL;

/**
 * Main class recieve text from url.
 * Count number of words ,syllables ,and compute the elasped time.
 * @author Prin Angkunanuwat
 *
 */
public class Main {
	public static void main(String[] args){
		StopWatch sw = new StopWatch();
		
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = null;
		try {
			url = new URL( DICT_URL );
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		WordCounter counter = new WordCounter();
		sw.start();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		sw.stop();
		System.out.println(String.format("Reading words from http://se.cpe.ku.ac.th/dictionary\nCounted %,d syllables in %,d words\nElapsed time: %.3f sec",syllables ,wordcount ,sw.getElapsed() ));
	}
}
