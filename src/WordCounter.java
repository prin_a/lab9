import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

/**
 * Word counter which count the word and syllables.
 * @author Prin Angkunanuwat
 *
 */
public class WordCounter {
	/** enum contain state.
	 *
	 */
	public enum State {
		START,
		CONSONANT,
		E_FIRST,
		VOWEL,
		DASH,
		NONWORD; 
	}
	private State state;
	private int syllablesCount;
	/**
	 * Count the syllables of the word.
	 * @param word is a string we need to count its syllables.
	 * @return count of syllables.
	 */
	public int countSyllables(String word) {

		int syllables = 0;
		setState(State.START);
		int count = 0;
		for( char c : word.toCharArray() ) { 
			count++;
			switch(state) {
			case START:
				if (isWhitespace(c)) break;
				else if (!isLetter(c)) setState(State.NONWORD);
				else if (c=='e' || c=='E') setState(State.E_FIRST);
				else if (isVowel(c) || c=='y' || c=='Y') setState(State.VOWEL);
				else if (isLetter(c)) setState(State.CONSONANT);
				break;
			case CONSONANT:
				if (c=='\'') break;
				else if (c=='-') setState(State.DASH);
				else if (!isLetter(c)) setState(State.NONWORD);
				else if (c=='e' || c=='E') setState(State.E_FIRST);
				else if (isVowel(c) || c=='y' || c=='Y') setState(State.VOWEL);
				else if (isLetter(c) && !isVowel(c)) setState(State.CONSONANT);
				break;
			case E_FIRST:
				if (c=='\'') break; 
				else if (c=='-') setState(State.DASH);
				else if (!isLetter(c)) setState(State.NONWORD);
				else if (isVowel(c)) setState(State.VOWEL);
				else if (isLetter(c) && !isVowel(c)) {
					syllables++;
					setState(State.CONSONANT);
				}
				break;
			case VOWEL:
				if (c=='\'') break;
				else if (c=='-') { 
					syllables++; 
					setState(State.DASH); 
				}
				else if (!isLetter(c)) setState(State.NONWORD);
				else if (isVowel(c) ) break;
				else if (isLetter(c) && !isVowel(c)) {
					syllables++;
					setState(State.CONSONANT);
				}
				break;
			case DASH:
				if (!isLetter(c)) setState(State.NONWORD);
				else if (c=='e' || c=='E') setState(State.E_FIRST);
				else if (isVowel(c) || c=='y' || c=='Y') setState(State.VOWEL);
				else if (isLetter(c) && !isVowel(c)) setState(State.CONSONANT);
				break;
			case NONWORD:
				return 0;
			}
		}
		if( state == State.DASH) return 0;
		if( state == State.VOWEL || state == State.E_FIRST && syllables ==0 ) syllables++;
		return syllables;

	}
	/**
	 * Check if it's vowel or not.
	 * @param c is character we want to check.
	 * @return true if c is vowel;
	 */
	private boolean isVowel(char c) {
		return "AEIOUaeiou".indexOf(c) >= 0; 

	}
	/**
	 * Check if c is letter or not.
	 * @param c is character we want to check.
	 * @return true if c is letter.
	 */
	private boolean isLetter(char c) {
		return Character.isLetter(c);
	}
	/**
	 * Check if c is space, tab, new line.
	 * @param c is character we want to check.
	 * @return true if c is space,tab, new line.
	 */
	private boolean isWhitespace(char c) {
		return Character.isWhitespace(c);
	}

	/**
	 * Set state.
	 * @param state we need to set.
	 */
	private void setState(State state){
		this.state = state;
	}
	/**
	 * Count the word from instream.
	 * @param instream we need to read.
	 * @return count of the word.
	 */
	public int countWords(InputStream instream) {
		BufferedReader breader = new BufferedReader( new InputStreamReader(instream) );
		String line;
		int count = 0;
		syllablesCount = 0;
		while(true){
			try {
				line = breader.readLine();
			} catch (IOException e1) {
				return -1;
			}
			if ( line == null ) break;
			StringTokenizer token =  new StringTokenizer(line);
			while(token.hasMoreTokens()){
				count++;
				syllablesCount += countSyllables(token.nextToken());
			}
		}
		return count;
	}
	/**
	 * Count the word from url.
	 * @param url that contain text.
	 * @return Count of the word in url.
	 */
	public int countWords(URL url) {
		InputStream in = null;
		try {
			in = url.openStream( );
		} catch (IOException e) {
			return -1;
		}
		return countWords(in);
	}
	/**
	 * Get syllables from recent countWords.
	 * @return syllables from recent countWords.
	 */
	public int getSyllableCount() {
		return syllablesCount;
	}

}
